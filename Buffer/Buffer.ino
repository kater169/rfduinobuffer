#include <stydlib.h>
#include <RFduinoBLE.h>

// JSON Chars
String startString = "{";
String endString = "}";
String assignString = ":";
String seperatorString = ",";
String quoteString = "'";
//Fields
String speedName = "speed";
String throttelName = "trottel";
String gearName = "gear";
String rpmName = "rpm";
String timeName = "time";
String tempName = "temp";

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  
  // this is the data we want to appear in the advertisement
  // (the deviceName length plus the advertisement length must be <= 18 bytes)
  RFduinoBLE.txPowerLevel = +4; 
  RFduinoBLE.advertisementData = "OBD";
  RFduinoBLE.advertisementInterval = 50;
  RFduinoBLE.deviceName = "Ekoio L-PH1";
  // start the BLE stack
  RFduinoBLE.begin();
}

void loop() 
{
  int speed = random(0, 180);
  int throttel = 0;
  int gear = random(-1, 6);
  int rpm = random(0, 8000);
  int temp = int(RFduino_temperature(CELSIUS)); 

  String obdData;
  obdData += startString;
  //setSpeadData
  obdData += speedName + assignString + speed + seperatorString;
  // set RpmData
  obdData += rpmName + assignString + rpm + seperatorString;
  // set GearData
  obdData += gearName + assignString + gear + seperatorString;
  // set ThrottleData
  obdData += throttelName + assignString + throttel + seperatorString;
  // timer
  obdData += timeName + assignString + millis() + seperatorString;
  // rfduino temperature
  obdData += tempName + assignString + temp + seperatorString;
  // close JSON 
  obdData += endString;  

  unsigned int len =  obdData.length();
  char data[len];

  obdData.toCharArray(data, len);
  
  // put your main code here, to run repeatedly:
  int dataSize = 0;
  int bufferSize = 12; 
  uint8_t buffer[bufferSize];   
  int index = 0;
  int i;
      
  while (i < len) {
   
    buffer[index] = data[index];
    index++;
    if (index == bufferSize) {
      dataSize += bufferSize;
      
      // print some output for debugging
      Serial.print("+");
      if (dataSize % (bufferSize * 12 * 6) == 0) { 
        Serial.println(""); // linefeed 
      } 
      
      // send the data
      RFduinoBLE.send((char*)buffer, bufferSize);
      Serial.println((char*)buffer);
      // reset buffer
      index = 0;
      memset(buffer, 0, bufferSize);
      
      delay(30); // larger files need a bigger delay
    }
    i++;
  }
      
  if (index > 0) {
    dataSize += index;
    Serial.println("Sending final chunk");
    RFduinoBLE.send((char*)buffer, bufferSize);
  }
  Serial.println("File data sent.");
  i = 0;
  // send unique token to denote the end of file
  char *token = "QED";
  Serial.print("Sending end token ");Serial.println(token);
  RFduinoBLE.send(token, sizeof(token));

  Serial.print("Finished. Sent ");
  Serial.print(dataSize);
  Serial.println(" bytes.");

}
